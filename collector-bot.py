#!/usr/bin/python3

print(
    "################ Remember to run this as the same user you use for jumphost #####################"
)

# python standard modules
import getpass
import multiprocessing


# 3rd party modules
from netmiko import BaseConnection


# custom modules
from models import device, interface, result
import settings

# provider import
# import test_provider as provider
import infoblox_provider as provider


# get credentials
username = input("username: ")
password = getpass.getpass()


device_provider = provider.provider(username, password)


def per_device_action(device_object):
    current_device = device.device(device_object["name"])
    try:
        connection_profile = {
            "device_type": "cisco_ios",
            "ip": device_object["name"],
            "username": username,
            "password": password,
        }

        # add jumphost if nesseary
        if device_object["jumphost"]:
            connection_profile["ssh_config_file"] = "jumphost_ssh_config"

        connection = BaseConnection(**connection_profile)

        ########################## Platform and software collection start ##########################

        # get the line with active switch and software in it from show ver
        show_ver_filtered = connection.send_command("sh ver | i \*")

        # generate a list of fields from filtered show ver
        sv_fields = []
        for s in show_ver_filtered.split(" "):
            if s is not "*" and s is not "":
                sv_fields.append(s)

        # set platform and software version
        current_device.platform = sv_fields[2]
        current_device.software_version = sv_fields[3]

        ########################## Platform and software collection end ##########################

        ########################## interface and cdp collection start ##########################

        interface_list = []

        # get interfaces
        raw_interface_list = connection.send_command("show ip interface brief")

        # remove title line
        il = raw_interface_list.split("\n")
        del il[:1]
        for i in il:

            int_fields = []
            for s in i.split(" "):
                if s is not "":
                    int_fields.append(s)

            int_name = int_fields[0]
            link_status = int_fields[4]

            show_int = connection.send_command(f"show int {int_name}")

            reliability = None
            packet_input = None
            packet_output = None
            unknown_protocol_drops = None
            total_output_drops = None

            for line in show_int.split("\n"):
                # std field split
                fields = []
                for s in line.split(" "):
                    if s is not "":
                        fields.append(s)

                if fields[0] is "reliability":
                    reliability = fields[1].split("/")[0]

                if "packets input" in line:
                    packet_input = fields[0]

                if "packets output" in line:
                    packet_output = fields[0]

                if "unknown protocol drops" in line:
                    unknown_protocol_drops = fields[0]

                if "Total output drops" in line:
                    total_output_drops = fields[7]

            # get cdp ne
            cdp_ne_names_raw = connection.send_command(
                f"sh cdp ne {int_name} de | i Device ID"
            )
            cdp_ne_names = []
            if cdp_ne_names_raw is not "":
                for l in cdp_ne_names_raw.split("\n"):
                    if "Invalid input" not in l and "^" not in l and l is not "":
                        cdp_ne_names.append(l.replace("Device ID: ", ""))

            # create interface object
            int_show_run = connection.send_command(f"show run int {int_name}")
            if "switchport mode access" in int_show_run:
                vlan = 1
                for line in int_show_run.split("\n"):
                    if "switchport access vlan" in line:
                        try:
                            vlan = int(line.replace("switchport access vlan ", ""))
                        except:
                            pass
                port = interface.access_port(
                    name=int_name,
                    cdp_neighbors=cdp_ne_names,
                    link_status=link_status,
                    reliability=reliability,
                    packet_input=packet_input,
                    packet_output=packet_output,
                    unknown_protocol_drops=unknown_protocol_drops,
                    total_output_drops=total_output_drops,
                    vlan=vlan,
                )
                interface_list.append(port)
            else:
                port = interface.trunk_port(
                    name=int_name,
                    cdp_neighbors=cdp_ne_names,
                    link_status=link_status,
                    reliability=reliability,
                    packet_input=packet_input,
                    packet_output=packet_output,
                    unknown_protocol_drops=unknown_protocol_drops,
                    total_output_drops=total_output_drops,
                )
                interface_list.append(port)

        current_device.interfaces = interface_list
        ########################## interface and cdp collection start ##########################

    except Exception as e:
        current_device.reachable = False
        print(f"############## {device_object['name']} failed ##############\n {e}")
    return current_device


# get devices
device_list = device_provider.get()

# mp
mp = multiprocessing.Pool(128)
results = mp.map(per_device_action, device_list)


pol = []


# all interface lines created and appended to pol
for r in results:
    for i in r.interfaces:
        vl = "Trunk"
        try:
            vl = i.vlan
        except:
            pass

        res = result.line(
            hostname=r.hostname,
            int_name=i.name,
            platform=r.platform,
            software=r.software_version,
            link_status=i.link_status,
            reliability=i.reliability,
            packet_intput=i.packet_input,
            packet_output=i.packet_output,
            unknown_protocol_drops=i.unknown_protocol_drops,
            total_output_drops=i.total_output_drops,
            drop_procentage=i.output_drop_procentage(),
            cdp_ne=i.cdp_neighbors,
            vlan=vl,
        )

        pol.append(res)


header_line = result.line(
    hostname="Hostname",
    int_name="Interface",
    platform="Platform",
    software="Software",
    link_status="Link status",
    reliability="Reliability",
    packet_intput="Packet input",
    packet_output="Packet output",
    unknown_protocol_drops="Unknown protocol drops",
    total_output_drops="Total output drops",
    drop_procentage="Unknown drop ",
    cdp_ne="CDP List",
    vlan="Vlan/trunk",
)


all_port_output = header_line.std()
cdp_port_output = header_line.cdp()


for p in pol:
    all_port_output += p.std()
    cdp_port_output += p.cdp()


std_out = open("all_ports.csv", "w")
std_out.write(all_port_output)
std_out.close()

cdp_out = open("cdp_ports.csv", "w")
cdp_out.write(cdp_port_output)
cdp_out.close()

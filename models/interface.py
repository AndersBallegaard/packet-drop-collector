class interface:
    def __init__(
        self,
        name,
        cdp_neighbors,
        link_status,
        reliability,
        packet_input,
        packet_output,
        unknown_protocol_drops,
        total_output_drops,
    ):
        self.name = name
        self.cdp_neighbors = cdp_neighbors
        self.link_status = link_status
        self.reliability = reliability
        self.packet_input = packet_input
        self.packet_output = packet_output
        self.unknown_protocol_drops = unknown_protocol_drops
        self.total_output_drops = total_output_drops

    def output_drop_procentage(self):
        i = "Error"
        try:
            i = int(self.packet_input) * 100 / int(self.packet_output)
        except:
            pass
        return i


class access_port(interface):
    def __init__(
        self,
        name,
        cdp_neighbors,
        link_status,
        reliability,
        packet_input,
        packet_output,
        unknown_protocol_drops,
        total_output_drops,
        vlan,
    ):
        self.name = name
        self.cdp_neighbors = cdp_neighbors
        self.link_status = link_status
        self.reliability = reliability
        self.packet_input = packet_input
        self.packet_output = packet_output
        self.unknown_protocol_drops = unknown_protocol_drops
        self.total_output_drops = total_output_drops
        self.vlan = vlan


class trunk_port(interface):
    def __init__(
        self,
        name,
        cdp_neighbors,
        link_status,
        reliability,
        packet_input,
        packet_output,
        unknown_protocol_drops,
        total_output_drops,
    ):
        self.name = name
        self.cdp_neighbors = cdp_neighbors
        self.link_status = link_status
        self.reliability = reliability
        self.packet_input = packet_input
        self.packet_output = packet_output
        self.unknown_protocol_drops = unknown_protocol_drops
        self.total_output_drops = total_output_drops

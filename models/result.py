class line:
    def __init__(
        self,
        hostname,
        int_name,
        platform,
        software,
        link_status,
        reliability,
        packet_intput,
        packet_output,
        unknown_protocol_drops,
        total_output_drops,
        drop_procentage,
        cdp_ne,
        vlan,
    ):
        self.hostname = hostname
        self.int_name = int_name
        self.platform = platform
        self.software = software
        self.link_status = link_status
        self.reliability = reliability
        self.packet_intput = packet_intput
        self.packet_output = packet_output
        self.unknown_protocol_drops = unknown_protocol_drops
        self.total_output_drops = total_output_drops
        self.drop_procentage = drop_procentage
        self.cdp_ne = cdp_ne
        self.vlan = vlan

    def std(self):
        return f"{self.hostname},{self.int_name},{self.platform},{self.software},{self.link_status},{self.reliability},{self.packet_intput},{self.packet_output},{self.unknown_protocol_drops},{self.drop_procentage},{self.total_output_drops},{self.vlan}\n"

    def cdp(self):
        r = ""
        if self.cdp_ne != []:
            r = f"{self.hostname},{self.int_name},{self.platform},{self.software},{self.link_status},{self.reliability},{self.packet_intput},{self.packet_output},{self.unknown_protocol_drops},{self.drop_procentage},{self.total_output_drops},{self.vlan},{self.cdp_ne}\n"
        return r

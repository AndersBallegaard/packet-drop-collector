class device:
    def __init__(self, hostname):
        self.hostname = hostname
        self.software_version = None
        self.platform = None
        self.interfaces = []
        self.reachable = True

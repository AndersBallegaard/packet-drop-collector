#General
if jumphost is required add ssh keys between jumphost and current host

Because of the way it does jumphosting you need to run as your admin account on the local machine

That can be done with
```
ssh-keygen




ssh-copy-id jumphost.example.com
```

sudo apt-get install build-essential libssl-dev libffi-dev python3-dev

# Provider api
A device provider needs to have implemented a class called provider that support the following:
* A __init__ function that takes a username and password
* A function called get() without any arguments that return a list of devices in the following format: 

```
[
    {
        "name" : "device-A",
        "jumphost" : False
    },
    {
        "name" : "device-B",
        "jumphost" : True
    }
]
    
```

#Device model
A device model have the following features
* A list of interfaces using the interface model (empty if unreacable)
* A hostname
* A reachability status
* software version
* platform

#Interface Model
An interface model have the following features
* An interface name
* A list of CDP neighbors (empty if none)
* A link status indication
* Reachability (int between 1 and 255)
* packet input
* packet output
* unknown protocol drops
* total output drops